package pe.academiamoviles.academiamarket

import android.app.Application
import coil.Coil
import coil.ImageLoader
import coil.util.CoilUtils
import okhttp3.OkHttpClient
import pe.academiamoviles.academiamarket.data.local.Database
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        Database.inicializar(this)
        initCoil()

        /**
         * SÓLO si la aplicación se está debuggeando (cuando se compila desde el IDE)
         * registrará los logs de Timber en el LogCat
         */
        if (BuildConfig.DEBUG) {
            initTimber()
            // ..
        }
    }

    /**
     * Coil es el encargado de cargar imágenes desde una URL.
     * Aquí sólo se detallan parámetros de su configuración inicial
     */
    private fun initCoil() {
        Coil.setDefaultImageLoader {
            ImageLoader(this) {
                crossfade(true)
                okHttpClient {
                    OkHttpClient.Builder()
                        .cache(CoilUtils.createDefaultCache(this@App))
                        .build()
                }
            }
        }
    }

    /**
     * Timber es una simple logger
     */
    private fun initTimber() {
        Timber.plant(Timber.DebugTree())
    }

}