package pe.academiamoviles.academiamarket.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import pe.academiamoviles.academiamarket.data.local.dao.ProductoDao
import pe.academiamoviles.academiamarket.model.Producto

@Database(
    entities = [
        Producto::class
    ], version = 1
)
abstract class AcademiaMarketDatabase : RoomDatabase() {

    abstract fun productoDao(): ProductoDao

}