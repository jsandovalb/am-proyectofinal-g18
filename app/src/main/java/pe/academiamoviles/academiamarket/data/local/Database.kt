package pe.academiamoviles.academiamarket.data.local

import android.content.Context
import androidx.room.Room
import kotlinx.coroutines.CoroutineScope

object Database {

    private lateinit var database: AcademiaMarketDatabase

    /**
     * No es necesario "inicializar" la base de datos con la opción allowMainThreadQueries()
     * porque se realizará el manejo de datos de a través de una Coroutine
     * @see [CoroutineScope]
     *
     * Se deja comentado el método "allowMainThreadQueries()"
     */
    fun inicializar(context: Context) {
        if (!this::database.isInitialized) { // si NO está inicializada la variable
            database = Room.databaseBuilder(
                context,
                AcademiaMarketDatabase::class.java,
                "AcademiaMarket_DB"
            )
                //.allowMainThreadQueries()
                .build()
        }
    }

    val productoDao by lazy { database.productoDao() }
}