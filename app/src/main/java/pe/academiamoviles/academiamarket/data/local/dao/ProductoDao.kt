package pe.academiamoviles.academiamarket.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import pe.academiamoviles.academiamarket.model.Producto

@Dao
interface ProductoDao {

    @Insert
    fun insertarProducto(producto: Producto)

    @Query("SELECT * FROM carrito_compras WHERE id = :productoId")
    fun obtenerProductoPorid(productoId: String): Producto?

    /**
     * Se puede llamar al listado de productos a través de [LiveData]
     * así obtenemos en tiempo real el cambio que sucede en esta tabla
     * para realizar X operación
     */

    @Query("SELECT * FROM carrito_compras")
    fun obtenerProductos(): LiveData<List<Producto>>

    @Delete
    fun eliminarProducto(producto: Producto)

    @Query("DELETE FROM carrito_compras")
    fun eliminarTodo()
}