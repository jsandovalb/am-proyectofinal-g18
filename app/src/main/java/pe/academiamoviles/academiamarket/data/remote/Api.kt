package pe.academiamoviles.academiamarket.data.remote

import pe.academiamoviles.academiamarket.data.remote.request.PagarCarritoRequest
import pe.academiamoviles.academiamarket.model.Categoria
import pe.academiamoviles.academiamarket.model.Producto
import pe.academiamoviles.academiamarket.model.Status
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface Api {

    @GET("productos")
    suspend fun obtenerProductos(): List<Producto>

    @GET("productos/porRanking")
    suspend fun obtenerRankingProductos(): List<Producto>

    @GET("productos/porCategoria/{idCategoria}")
    suspend fun obtenerProductosPorCategoria(@Path("idCategoria") idCategoria: String): List<Producto>

    @GET("categorias")
    suspend fun obtenerCategorias(): List<Categoria>

    @POST("carrito/pagar")
    suspend fun pagarCarritoCompras(@Body request: PagarCarritoRequest): Status

}