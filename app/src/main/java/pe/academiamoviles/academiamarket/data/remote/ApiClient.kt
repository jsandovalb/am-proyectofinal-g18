package pe.academiamoviles.academiamarket.data.remote

import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import okhttp3.OkHttpClient
import pe.academiamoviles.academiamarket.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

object ApiClient {

    val api: Api by lazy {
        Timber.i("creating api service...")
        val okHttpClient = createOkHttpClient()
        return@lazy createRetrofitClient(okHttpClient).create(Api::class.java)
    }

    /**
     * [LoggingInterceptor] ayudará a mapear en el Logcat los datos que se envían y reciben desde el servicio web
     */
    private fun createOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(
                LoggingInterceptor.Builder()
                    .loggable(BuildConfig.DEBUG) // sólo se mostrará si ejecutamos la app en modo debug
                    .setLevel(Level.HEADERS)
                    .setLevel(Level.BODY)
                    .build()
            )
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .callTimeout(30, TimeUnit.SECONDS)
            .build()

    private fun createRetrofitClient(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://academia-market.herokuapp.com/")
            .client(okHttpClient)
            .build()

}