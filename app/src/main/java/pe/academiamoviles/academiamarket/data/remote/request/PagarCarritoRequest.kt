package pe.academiamoviles.academiamarket.data.remote.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import pe.academiamoviles.academiamarket.model.Producto

data class PagarCarritoRequest(
    @SerializedName("productos") @Expose
    val productos: List<Producto>? = null
)