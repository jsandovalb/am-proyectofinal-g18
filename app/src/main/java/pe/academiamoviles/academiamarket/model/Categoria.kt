package pe.academiamoviles.academiamarket.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Categoria(
    @SerializedName("id") @Expose
    val id: String = "",

    @SerializedName("nombre") @Expose
    val nombre: String? = "",

    @SerializedName("imagenUrl") @Expose
    val imagenUrl: String? = ""
) : Parcelable