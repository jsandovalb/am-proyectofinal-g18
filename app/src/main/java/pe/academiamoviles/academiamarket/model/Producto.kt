package pe.academiamoviles.academiamarket.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.ColumnInfo
import androidx.room.Ignore
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Este data class se utiliza para 2 capas de la aplicación:
 * DATOS: entidad de base de datos Y responde del servicio web
 * PRESENTACION: mostrar listado en las pantallas respectivas
 *
 * También se pueden utilizar data class/es para cada capa en específico
 * @see (Revisar Clean Architecture)[https://devexperto.com/clean-architecture-android/]
 *
 * La entidad se llamará "carrito_compras" porque SÓLO se utilizará para almacenar productos
 * para el carrito.
 * NO se utilizará para guardar toda la lista de productos proveniente de los servicios web
 */

@Parcelize
@Entity(tableName = "carrito_compras")
data class Producto(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id") @Expose
    var id: String = "",

    @ColumnInfo
    @SerializedName("nombre") @Expose
    var nombre: String? = "",

    @ColumnInfo
    @SerializedName("descripcion") @Expose
    var descripcion: String? = "",

    @ColumnInfo
    @SerializedName("imagenUrl") @Expose
    var imagenUrl: String? = "",

    @ColumnInfo
    @SerializedName("precio") @Expose
    var precio: Int = 0,

    /**
     * [Ignore] se utiliza para que la columna/campo NO sea considerada por Room
     */

    @Ignore
    @SerializedName("categoria") @Expose
    val categoria: Categoria? = null
) : Parcelable {

    fun obtenerPrecioMostrar(): String {
        return "S/ ${String.format("%.2f", precio.toDouble())}"
    }

}