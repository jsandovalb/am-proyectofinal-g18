package pe.academiamoviles.academiamarket.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Status(
    @SerializedName("success") @Expose
    val success: Boolean = false,

    @SerializedName("mensaje") @Expose
    val mensaje: String? = null
)