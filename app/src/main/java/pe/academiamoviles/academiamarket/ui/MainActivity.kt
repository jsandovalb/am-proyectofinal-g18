package pe.academiamoviles.academiamarket.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*
import pe.academiamoviles.academiamarket.R
import pe.academiamoviles.academiamarket.model.Categoria
import pe.academiamoviles.academiamarket.model.Producto
import pe.academiamoviles.academiamarket.ui.contenido.tienda.TiendaViewModel
import pe.academiamoviles.academiamarket.ui.contenido.tienda.productos.ProductosCategoriaFragment
import timber.log.Timber

class MainActivity : AppCompatActivity() {

    private val navController by lazy { findNavController(R.id.fragmentContainer) }

    private val tiendaViewModel by lazy { ViewModelProvider(this).get(TiendaViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        setTitle(R.string.app_name)

        inicializarMenu()
        inicializarViewModel()
    }

    /**
     * La actividad principal es la que consulta UNA SOLA VEZ, los datos de "categorias" y "ranking de productos"
     * cada fragmento, de manera, interna, tiene la instancia del ViewModel generada por la actividad, y sólo observa los datos ya recolectados
     * a excepción del [ProductosCategoriaFragment], que consulta la lista de productos por categoría
     */
    private fun inicializarViewModel() {
        tiendaViewModel.obtenerRankingProductos()
        tiendaViewModel.obtenerCategorias()
    }

    private fun inicializarMenu() {
        bottomNavigationView.setupWithNavController(navController)

        /**
         * Alguna lógica "extra" para mostrar u ocultar el título con el botón "back"
         *
         * Pero fácilmente se puede colocar el título en cada fragmento
         */

        navController.addOnDestinationChangedListener { _, destination, arguments ->
            Timber.i("arguments: $arguments")

            mostrarOcultarBotonBack(false)
            val nuevoTitulo = when (destination.id) {
                R.id.actionCarrito -> getString(R.string.carrito_de_compras)
                R.id.actionContacto -> getString(R.string.contacto)
                R.id.actionCategorias -> {
                    mostrarOcultarBotonBack(true)
                    getString(R.string.categorias)
                }
                R.id.actionProductosCategoria -> {  // el argumento que se recibe para este caso es de tipo Categoria (declarado en el archivo de navegacion)
                    mostrarOcultarBotonBack(true)
                    arguments?.let {
                        (arguments.getParcelable<Categoria>("categoria"))?.nombre
                    } ?: ""
                }
                R.id.actionDetalleProducto -> { // el argumento que se recibe para este caso es de tipo Producto (declarado en el archivo de navegacion)
                    mostrarOcultarBotonBack(true)
                    arguments?.let {
                        (arguments.getParcelable<Producto>("producto"))?.nombre
                    } ?: ""
                }
                else -> getString(R.string.app_name)
            }
            title = nuevoTitulo
        }
    }

    private fun mostrarOcultarBotonBack(mostrar: Boolean) {
        if (mostrar) {
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        } else {
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                navController.navigateUp()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
