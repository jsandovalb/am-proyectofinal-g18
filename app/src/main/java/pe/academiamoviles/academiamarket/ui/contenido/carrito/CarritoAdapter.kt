package pe.academiamoviles.academiamarket.ui.contenido.carrito

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import kotlinx.android.synthetic.main.item_producto_carrito_compras.view.*
import pe.academiamoviles.academiamarket.R
import pe.academiamoviles.academiamarket.model.Producto

class CarritoAdapter(
    private val clickListener: ClickListener
) : RecyclerView.Adapter<CarritoAdapter.CarritoViewHolder>() {

    var productosCarrito = listOf<Producto>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarritoViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_producto_carrito_compras, parent, false)
        return CarritoViewHolder(view, clickListener)
    }

    override fun getItemCount(): Int = productosCarrito.size
    private fun getProducto(position: Int): Producto = productosCarrito[position]

    override fun onBindViewHolder(holder: CarritoViewHolder, position: Int) {
        holder.mostrarDatos(getProducto(position))
    }

    class CarritoViewHolder(private val view: View, private val clickListener: ClickListener) : RecyclerView.ViewHolder(view) {
        fun mostrarDatos(producto: Producto) {
            view.ivImagenProducto.load(producto.imagenUrl)
            view.tvNombreProducto.text = producto.nombre
            view.tvPrecio.text = producto.obtenerPrecioMostrar()

            /**
             * Evento para eliminar el producto del carrito de compras
             */
            view.ivEliminarProducto.setOnClickListener { clickListener.eliminarProducto(producto) }
        }
    }

    interface ClickListener {
        fun eliminarProducto(producto: Producto)
    }

}