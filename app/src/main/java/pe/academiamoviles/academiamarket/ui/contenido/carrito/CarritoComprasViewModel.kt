package pe.academiamoviles.academiamarket.ui.contenido.carrito

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import pe.academiamoviles.academiamarket.data.local.Database
import pe.academiamoviles.academiamarket.data.local.dao.ProductoDao
import pe.academiamoviles.academiamarket.data.remote.ApiClient
import pe.academiamoviles.academiamarket.data.remote.request.PagarCarritoRequest
import pe.academiamoviles.academiamarket.model.Producto
import timber.log.Timber


/**
 * [viewModelScope] es el encargado de "ejecutar" una Coroutine dentro de un [ViewModel]
 *
 * También se puede usar la clase común
 *          CoroutineScope(Dispatchers.IO).launch {  }
 *
 *
 * Las variables que tienen get() , sólo son llamadas a través del Fragmento que lo observa,
 * no pueden ser operadas en este ViewModel
 */

class CarritoComprasViewModel : ViewModel() {

    /**
     * sólo se creará el objeto cuando se utilice por primera vez, luego se reutilizará
     */
    private val productoDao by lazy { Database.productoDao }
    private val api by lazy { ApiClient.api }

    /**
     * Listado de productos del carrito de compras
     * Podemos llamar directamente al método de [ProductoDao] porque éste retorna un LiveData de un listado de productos :)
     */
    private var _productosCarrito = MutableLiveData<List<Producto>>()
    val productosCarrito: LiveData<List<Producto>>
        get() = _productosCarrito

    /**
     * Esta variable se utiliza para notificar el pago de los productos seleccionados, en el carrito de compras
     */
    private var _notificarPago = MutableLiveData<String>()
    val notificarPago: LiveData<String> = _notificarPago

    /**
     * Inicializar cualquier proceso, cuando se "instancia" este ViewModel
     */

    init {
        productoDao.obtenerProductos().observeForever { productos ->
            _productosCarrito.postValue(productos)
        }
    }

    fun eliminarProductoDelCarrito(producto: Producto) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                // verificar si existe el producto en el carrito (por si acaso)
                productoDao.obtenerProductoPorid(producto.id)?.let {
                    productoDao.eliminarProducto(producto)
                }
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    fun pagarProductos() {
        viewModelScope.launch(Dispatchers.IO) {
            productosCarrito.value?.let { productos ->   // se obtiene el listado "actual" de los productos
                try {
                    val request = PagarCarritoRequest(productos)
                    val response = api.pagarCarritoCompras(request)
                    if (response.success) {
                        eliminarTodoElCarrito()
                    }
                    _notificarPago.postValue(response.mensaje)
                } catch (e: Exception) {
                    Timber.e(e)
                }
            }
        }
    }

    private fun eliminarTodoElCarrito() {
        viewModelScope.launch(Dispatchers.IO) {
            productoDao.eliminarTodo()
        }
    }

}