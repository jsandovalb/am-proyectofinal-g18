package pe.academiamoviles.academiamarket.ui.contenido.carrito

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.thekhaeng.recyclerviewmargin.LayoutMarginDecoration
import kotlinx.android.synthetic.main.fragment_carrito.*
import pe.academiamoviles.academiamarket.R
import pe.academiamoviles.academiamarket.model.Producto
import pe.academiamoviles.academiamarket.ui.contenido.tienda.TiendaViewModel
import pe.academiamoviles.academiamarket.util.mostrarMensaje
import timber.log.Timber

class CarritoFragment : Fragment(), CarritoAdapter.ClickListener {

    /**
     * En este caso, sólo es necesario tener una sóla instancia de este viewmodel para este fragmento
     */
    private val carritoViewModel by lazy { ViewModelProvider(this).get(CarritoComprasViewModel::class.java) }

    private val carritoAdapter by lazy { CarritoAdapter(this) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_carrito, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inicializarElementos()
        inicializarViewModel()
    }

    private fun inicializarViewModel() {
        carritoViewModel.productosCarrito.observe(viewLifecycleOwner, Observer { productos ->
            carritoAdapter.productosCarrito = productos
            val carritoVacio = productos.isEmpty()
            contenidoPrincipal.visibility = if (carritoVacio) View.GONE else View.VISIBLE
            tvNoHayProductos.visibility = if (carritoVacio) View.VISIBLE else View.GONE
            obtenerPrecioTotal(productos)
        })

        carritoViewModel.notificarPago.observe(viewLifecycleOwner, Observer { mensaje ->
            activity?.mostrarMensaje(mensaje)
        })
    }

    private fun obtenerPrecioTotal(productos: List<Producto>) {
        val total = productos.sumBy { producto -> producto.precio }
        tvPrecioTotal.text = context?.getString(R.string.x_total, String.format("%.2f", total.toDouble()))
    }

    private fun inicializarElementos() {
        rvCarrito.layoutManager = LinearLayoutManager(context)
        rvCarrito.adapter = carritoAdapter

        /**
         * uso de una libreria agregada (ver archivo build.gradle)
         * ayuda a colocar "espacios" entre los elementos de la lista
         * @see [RecyclerView Margin Decoration](https://github.com/TheKhaeng/recycler-view-margin-decoration)
         */
        rvCarrito.addItemDecoration(LayoutMarginDecoration(1, 20))

        btnPagar.setOnClickListener {
            carritoViewModel.pagarProductos()
        }
    }

    override fun eliminarProducto(producto: Producto) {
        carritoViewModel.eliminarProductoDelCarrito(producto)
    }
}