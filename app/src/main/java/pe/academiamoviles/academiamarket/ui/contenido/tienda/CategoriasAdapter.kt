package pe.academiamoviles.academiamarket.ui.contenido.tienda

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import kotlinx.android.synthetic.main.item_categoria.view.*
import pe.academiamoviles.academiamarket.model.Categoria

/**
 * @param layoutRes: es usado para indicar cuál layout se utilizará para mostrar las categorías
 */
class CategoriasAdapter(
    private val clickListener: ClickListener,
    @LayoutRes private val layoutRes: Int
) : RecyclerView.Adapter<CategoriasAdapter.CategoriaViewHolder>() {

    /**
     * notificar que la lista ha cambiado, cada vez que se le asigna un valor
     */
    var categorias = listOf<Categoria>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriaViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(layoutRes, parent, false)
        return CategoriaViewHolder(view, clickListener)
    }

    override fun getItemCount(): Int = categorias.size

    /**
     * Obtener una categoría a través de la posición en el array
     */
    private fun getCategoria(position: Int): Categoria = categorias[position]

    override fun onBindViewHolder(holder: CategoriaViewHolder, position: Int) {
        val categoria = getCategoria(position)
        holder.mostrarDatos(categoria)
    }

    class CategoriaViewHolder(private val view: View, private val clickListener: ClickListener) : RecyclerView.ViewHolder(view) {
        fun mostrarDatos(categoria: Categoria) {
            view.tvNombreCategoria.text = categoria.nombre
            view.ivImagenCategoria.load(categoria.imagenUrl)
            view.setOnClickListener {
                clickListener.onCategoriaClicked(categoria)
            }
        }
    }

    /**
     * evento [ClickListener] utilizado en cada celda
     */
    interface ClickListener {
        fun onCategoriaClicked(categoria: Categoria)
    }

}