package pe.academiamoviles.academiamarket.ui.contenido.tienda

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.thekhaeng.recyclerviewmargin.LayoutMarginDecoration
import kotlinx.android.synthetic.main.fragment_categorias.*
import kotlinx.android.synthetic.main.fragment_categorias.rvCategorias
import kotlinx.android.synthetic.main.fragment_tienda.*
import pe.academiamoviles.academiamarket.R
import pe.academiamoviles.academiamarket.model.Categoria

class CategoriasFragment : Fragment(), CategoriasAdapter.ClickListener {

    private val navController by lazy { findNavController() }

    private val categoriesAdapter by lazy { CategoriasAdapter(this, R.layout.item_categoria_grande) }
    private lateinit var tiendaViewModel: TiendaViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_categorias, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        inicializarElementos()
        inicializarViewModel()
    }

    private fun inicializarViewModel() {
        tiendaViewModel = activity?.run {
            ViewModelProvider(this).get(TiendaViewModel::class.java)
        } ?: throw Exception("fallo al inicializar el viewModel")

        tiendaViewModel.categorias.observe(viewLifecycleOwner, Observer { categorias ->
            categoriesAdapter.categorias = categorias
        })
    }

    private fun inicializarElementos() {
        rvCategorias.layoutManager = GridLayoutManager(context, 2)
        rvCategorias.adapter = categoriesAdapter

        /**
         * uso de una libreria agregada (ver archivo build.gradle)
         * ayuda a colocar "espacios" entre los elementos de la lista
         * @see [RecyclerView Margin Decoration](https://github.com/TheKhaeng/recycler-view-margin-decoration)
         */
        rvCategorias.addItemDecoration(LayoutMarginDecoration(2, 30))
    }

    override fun onCategoriaClicked(categoria: Categoria) {
        val action = CategoriasFragmentDirections.actionActionCategoriasToProductosCategoriaFragment(categoria)
        navController.navigate(action)
    }

}