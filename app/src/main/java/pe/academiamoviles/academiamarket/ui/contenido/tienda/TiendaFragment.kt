package pe.academiamoviles.academiamarket.ui.contenido.tienda

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.thekhaeng.recyclerviewmargin.LayoutMarginDecoration
import kotlinx.android.synthetic.main.fragment_tienda.*
import pe.academiamoviles.academiamarket.R
import pe.academiamoviles.academiamarket.model.Categoria
import pe.academiamoviles.academiamarket.model.Producto
import pe.academiamoviles.academiamarket.ui.MainActivity
import pe.academiamoviles.academiamarket.ui.contenido.tienda.productos.ProductosAdapter
import pe.academiamoviles.academiamarket.util.simpleToast
import timber.log.Timber

class TiendaFragment : Fragment(), CategoriasAdapter.ClickListener, ProductosAdapter.ClickListener {

    private val navController by lazy { findNavController() }

    private lateinit var tiendaViewModel: TiendaViewModel
    private val productosAdapter by lazy { ProductosAdapter(this, R.layout.item_producto) }
    private val categoriasAdapter by lazy { CategoriasAdapter(this, R.layout.item_categoria) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tienda, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inicializarElementos()
        inicializarViewModel()
    }


    /**
     * Se "instancia" el viewModel a nivel de la actividad padre: [MainActivity]
     */
    private fun inicializarViewModel() {
        tiendaViewModel = activity?.run {
            ViewModelProvider(this).get(TiendaViewModel::class.java)
        } ?: throw Exception("fallo al inicializar el viewModel")

        /**
         * se utiliza el atributo "visibility" de cualquier [View], para mostrar u ocultarlo
         */
        tiendaViewModel.loader.observe(viewLifecycleOwner, Observer { mostrar ->
            progressCategorias.visibility = if (mostrar) View.VISIBLE else View.GONE
            rvCategorias.visibility = if (mostrar) View.GONE else View.VISIBLE
        })
        tiendaViewModel.productosRanking.observe(viewLifecycleOwner, Observer { productos ->
            productosAdapter.productos = productos
        })
        tiendaViewModel.categorias.observe(viewLifecycleOwner, Observer { categorias ->
            categoriasAdapter.categorias = categorias
        })
        tiendaViewModel.productoInsertado.observe(viewLifecycleOwner, Observer { insertadoCorrectamente ->
            if (insertadoCorrectamente) simpleToast(R.string.producto_agregado)
        })

    }

    private fun inicializarElementos() {
        tvVerTodo.setOnClickListener { navController.navigate(R.id.actionTienda_to_categoriasFragment) }

        rvRankingProductos.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rvRankingProductos.adapter = productosAdapter

        rvCategorias.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rvCategorias.adapter = categoriasAdapter

        /**
         * uso de una libreria agregada (ver archivo build.gradle)
         * ayuda a colocar "espacios" entre los elementos de la lista
         * @see [RecyclerView Margin Decoration](https://github.com/TheKhaeng/recycler-view-margin-decoration)
         */
        rvRankingProductos.addItemDecoration(LayoutMarginDecoration(1, 30))
        rvCategorias.addItemDecoration(LayoutMarginDecoration(1, 30))
    }

    override fun onCategoriaClicked(categoria: Categoria) {
        Timber.i("categoria seleccionada: $categoria")
        val action = TiendaFragmentDirections.actionActionTiendaToActionProductosCategoria(categoria)
        navController.navigate(action)
    }

    override fun onProductoClicked(producto: Producto) {
        val action = TiendaFragmentDirections.actionTiendaToDetalleProductoFragment(producto)
        navController.navigate(action)
    }

    override fun agregarProducto(producto: Producto) {
        tiendaViewModel.agregarProductoAlCarrito(producto)
    }

}