package pe.academiamoviles.academiamarket.ui.contenido.tienda

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import pe.academiamoviles.academiamarket.data.local.Database
import pe.academiamoviles.academiamarket.data.remote.ApiClient
import pe.academiamoviles.academiamarket.model.Categoria
import pe.academiamoviles.academiamarket.model.Producto
import timber.log.Timber

/**
 * [viewModelScope] es el encargado de "ejecutar" una Coroutine dentro de un [ViewModel]
 *
 * También se puede usar la clase común
 *          CoroutineScope(Dispatchers.IO).launch {  }
 *
 *
 * Las variables que tienen get() , sólo son llamadas a través del Fragmento que lo observa,
 * no pueden ser operadas en este ViewModel
 */

class TiendaViewModel : ViewModel() {

    /**
     * sólo se creará el objeto cuando se utilice por primera vez, luego se reutilizará
     */
    private val productoDao by lazy { Database.productoDao }
    private val api by lazy { ApiClient.api }

    private var _loader = MutableLiveData<Boolean>()
    val loader: LiveData<Boolean>
        get() = _loader

    private var _categorias = MutableLiveData<List<Categoria>>()
    val categorias: LiveData<List<Categoria>>
        get() = _categorias

    /**
     * Listado de productos ordenados por el ranking de ventas
     */
    private var _productosRanking = MutableLiveData<List<Producto>>()
    val productosRanking: LiveData<List<Producto>>
        get() = _productosRanking

    /**
     * Listado de productos de una categoría
     */
    private val _productos = MutableLiveData<List<Producto>>()
    val productos: LiveData<List<Producto>>
        get() = _productos

    /**
     * Esta variable se utiliza para saber si un producto fue insertado correctamente o no
     */
    private var _productoInsertado = MutableLiveData<Boolean>()
    val productoInsertado: LiveData<Boolean>
        get() = _productoInsertado


    fun obtenerCategorias() {
        _loader.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _categorias.postValue(api.obtenerCategorias())
            } catch (e: Exception) {
                Timber.e(e)
            } finally {
                _loader.postValue(false)
            }
        }
    }

    fun obtenerProductosDeCategoria(idCategoria: String) {
        _loader.postValue(true)
        _productos.postValue(listOf()) // lista vacía
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _productos.postValue(api.obtenerProductosPorCategoria(idCategoria))
            } catch (e: Exception) {
                Timber.e(e)
            } finally {
                _loader.postValue(false)
            }
        }
    }

    fun obtenerRankingProductos() {
        _productosRanking.postValue(listOf()) // lista vacía
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _productosRanking.postValue(api.obtenerProductos())
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    fun agregarProductoAlCarrito(producto: Producto) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                // verificar si YA existe el producto en el carrito
                productoDao.obtenerProductoPorid(producto.id)?.let {
                    // ya existe el producto
                    Timber.i("este producto ya fue insertado en el carrito de compras ...")
                    _productoInsertado.postValue(false)

                } ?: run {
                    // NO existe en el carrito de compras, entonces insertarlo
                    Timber.i("insertando nuevo producto al carrito ...")
                    productoDao.insertarProducto(producto)
                    _productoInsertado.postValue(true)
                }
            } catch (e: Exception) {
                Timber.e(e)
            } finally {
                _productoInsertado.postValue(false)  //  "resetear"
            }
        }
    }

}