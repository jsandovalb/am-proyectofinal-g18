package pe.academiamoviles.academiamarket.ui.contenido.tienda.productos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import coil.api.load
import kotlinx.android.synthetic.main.fragment_detalle_producto.*
import pe.academiamoviles.academiamarket.R
import pe.academiamoviles.academiamarket.ui.contenido.tienda.TiendaViewModel
import pe.academiamoviles.academiamarket.util.simpleToast

class DetalleProductoFragment : Fragment() {

    private lateinit var tiendaViewModel: TiendaViewModel

    private val args: DetalleProductoFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_detalle_producto, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inicializarElementos()
        inicializarViewModel()
    }

    private fun inicializarViewModel() {
        tiendaViewModel = activity?.run {
            ViewModelProvider(this).get(TiendaViewModel::class.java)
        } ?: throw Exception("error al inicializar el viewModel")

        tiendaViewModel.productoInsertado.observe(viewLifecycleOwner, Observer { insertadoCorrectamente ->
            if (insertadoCorrectamente) simpleToast(R.string.producto_agregado)
        })
    }

    private fun inicializarElementos() {
        args.producto.let { producto ->
            tvNombreCategoria.text = producto.categoria?.nombre
            tvNombreProducto.text = producto.nombre
            tvPrecio.text = producto.obtenerPrecioMostrar()
            tvDescripcionProducto.text = producto.descripcion
            ivImagenProducto.load(producto.imagenUrl)
        }
        btnAgregarProducto.setOnClickListener {
            tiendaViewModel.agregarProductoAlCarrito(args.producto)
        }
    }

}