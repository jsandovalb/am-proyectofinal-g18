package pe.academiamoviles.academiamarket.ui.contenido.tienda.productos

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import kotlinx.android.synthetic.main.item_producto.view.*
import pe.academiamoviles.academiamarket.R
import pe.academiamoviles.academiamarket.model.Producto


/**
 * @param layoutRes: es usado para indicar cuál layout se utilizará para mostrar las categorías
 */
class ProductosAdapter(
    private val clickListener: ClickListener,
    @LayoutRes private val layoutRes: Int
) : RecyclerView.Adapter<ProductosAdapter.ProductoViewHolder>() {

    /**
     * notificar que la lista ha cambiado, cada vez que se le asigna un valor
     */
    var productos = listOf<Producto>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductoViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(layoutRes, parent, false)
        return ProductoViewHolder(view, clickListener)
    }

    override fun getItemCount(): Int = productos.size

    /**
     * Obtener un producto a través de la posición en el array
     */
    private fun getProducto(position: Int): Producto = productos[position]

    override fun onBindViewHolder(holder: ProductoViewHolder, position: Int) {
        val producto = getProducto(position)
        holder.mostrarDatos(producto)
    }

    class ProductoViewHolder(private val view: View, private val clickListener: ClickListener) : RecyclerView.ViewHolder(view) {
        fun mostrarDatos(producto: Producto) {
            view.ivImagenProducto.load(producto.imagenUrl)
            view.tvNombreCategoria.text = producto.categoria?.nombre
            view.tvNombreProducto.text = producto.nombre
            view.tvPrecio.text = producto.obtenerPrecioMostrar() // es una función declarada en el data class Producto

            /**
             * detalle del producto al seleccionar la imagen
             */
            view.ivImagenProducto.setOnClickListener { clickListener.onProductoClicked(producto) }


            /**
             * agregar producto al carrito
             */
            view.btnAgregarProducto.setOnClickListener { clickListener.agregarProducto(producto) }
        }

    }

    /**
     * evento [ClickListener] utilizado en cada celda
     */
    interface ClickListener {
        fun onProductoClicked(producto: Producto)
        fun agregarProducto(producto: Producto)
    }

}
