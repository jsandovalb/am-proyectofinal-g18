package pe.academiamoviles.academiamarket.ui.contenido.tienda.productos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.thekhaeng.recyclerviewmargin.LayoutMarginDecoration
import kotlinx.android.synthetic.main.fragment_categoria_productos.*
import pe.academiamoviles.academiamarket.R
import pe.academiamoviles.academiamarket.model.Producto
import pe.academiamoviles.academiamarket.ui.contenido.tienda.TiendaViewModel
import pe.academiamoviles.academiamarket.ui.MainActivity
import pe.academiamoviles.academiamarket.util.simpleToast

class ProductosCategoriaFragment : Fragment(), ProductosAdapter.ClickListener {

    private val navController by lazy { findNavController() }

    private val args: ProductosCategoriaFragmentArgs by navArgs()

    private val productosAdapter by lazy { ProductosAdapter(this, R.layout.item_producto_grande) }
    private lateinit var tiendaViewModel: TiendaViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_categoria_productos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inicializarElementos()
        inicializarViewModel()
    }

    /**
     * Se "instancia" el viewModel a nivel de la actividad padre: [MainActivity]
     */

    private fun inicializarViewModel() {
        tiendaViewModel = activity?.run {
            ViewModelProvider(this).get(TiendaViewModel::class.java)
        } ?: throw Exception("fallo al inicializar el viewModel")

        tiendaViewModel.loader.observe(viewLifecycleOwner, Observer { mostrar ->
            progressBar.visibility = if (mostrar) View.VISIBLE else View.GONE
            rvProductos.visibility = if (mostrar) View.GONE else View.VISIBLE
        })

        tiendaViewModel.productos.observe(viewLifecycleOwner, Observer { productos ->
            productosAdapter.productos = productos
        })

        tiendaViewModel.productoInsertado.observe(viewLifecycleOwner, Observer { insertadoCorrectamente ->
            if (insertadoCorrectamente) simpleToast(R.string.producto_agregado)
        })

        tiendaViewModel.obtenerProductosDeCategoria(args.categoria.id)
    }

    private fun inicializarElementos() {
        rvProductos.layoutManager = GridLayoutManager(context, 2)
        rvProductos.adapter = productosAdapter

        /**
         * uso de una libreria agregada (ver archivo build.gradle)
         * ayuda a colocar "espacios" entre los elementos de la lista
         * @see [RecyclerView Margin Decoration](https://github.com/TheKhaeng/recycler-view-margin-decoration)
         */
        rvProductos.addItemDecoration(LayoutMarginDecoration(2, 30))
    }

    override fun onProductoClicked(producto: Producto) {
        val action = ProductosCategoriaFragmentDirections.actionProductosCategoriaFragmentToActionDetalleProducto(producto)
        navController.navigate(action)
    }

    override fun agregarProducto(producto: Producto) {
        tiendaViewModel.agregarProductoAlCarrito(producto)
    }

}