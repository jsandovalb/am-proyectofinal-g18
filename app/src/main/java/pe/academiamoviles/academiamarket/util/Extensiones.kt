package pe.academiamoviles.academiamarket.util

import android.app.Activity
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import pe.academiamoviles.academiamarket.R

fun Activity.mostrarMensaje(mensaje: String) {
    AlertDialog.Builder(this)
        .setTitle(R.string.pago_realizado)
        .setMessage(mensaje)
        .setPositiveButton(R.string.finalizar, null)
        .show()
}

fun Fragment.simpleToast(@StringRes stringRes: Int) {
    Toast.makeText(context, stringRes, Toast.LENGTH_SHORT).show()
}